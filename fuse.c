#include <avr/io.h>

FUSES = {
    .low = 0xFF,
    .high = 0xCD,
    .extended = 0xFF
};
