# UPC Firmware
AVR Firmware for UPC (USB Pulse Counter). 
UPC is a 10 channel usb pulse counter. Intended to measuring S0 outputs from electricity meters. 
The hardware can be bought at [draketronic.de](http://www.draketronic.de).
The Firmware uses the V-USB library from [obdev.at](https://www.obdev.at/)
