/* Name: main.c
 * Project: UPC
 * Author: Maximilian Niedernhuber
 * Creation Date: 2019-02-24
 * License: GNU GPL v2 (see License.txt), GNU GPL v3
 */

#include <avr/io.h>
#include <avr/wdt.h>
#include <avr/interrupt.h>  /* for sei() */
#include <util/delay.h>     /* for _delay_ms() */
#include <util/atomic.h>
#include <avr/sleep.h>
#include <avr/eeprom.h>

#include <avr/pgmspace.h>   /* required by usbdrv.h */
#include <string.h>
#include "usbdrv.h"
#include "oddebug.h"        /* This is also an example for using debug macros */
#include "requests.h"       /* The custom request numbers we use */

/* ------------------------------------------------------------------------- */
/* ----------------------------- USB interface ----------------------------- */
/* ------------------------------------------------------------------------- */

volatile uint8_t pulseCount[8] = {0};


#define USB_PULSE_CODE_REQ 0
#define USB_EEPROM_READ    1
#define USB_EEPROM_WRITE   2
#define USB_REG_READ       3
#define USB_REG_WRITE      4


usbMsgLen_t usbFunctionSetup(uchar data[8])
{
usbRequest_t    *rq = (void *)data;
static uchar    dataBuffer[10];  /* buffer must stay valid when usbFunctionSetup returns */

    if(rq->bRequest == USB_PULSE_CODE_REQ){
        ATOMIC_BLOCK(ATOMIC_FORCEON) {
            memcpy(dataBuffer,pulseCount,8);
            memset(pulseCount,0,8);
        }
        //Lese Timer 0
        dataBuffer[8] = TCNT1L;
        TCNT1L = 0;

        //Lese Timer 1
        dataBuffer[9] = TCNT0;
        TCNT0 = 0;

        usbMsgPtr = dataBuffer;         /* tell the driver which data to return */

        UDR = 0; //Led Blinken

        return 10;                       /* tell the driver to send 1 byte */
    }
    else if(rq->bRequest == USB_EEPROM_READ){
        //Überprüfe Bereich
        if( rq->wValue.bytes[0] < 128 &&
            rq->wValue.bytes[1]  < 10 &&
            rq->wValue.bytes[0] + rq->wValue.bytes[1]  < 128 ){
            eeprom_read_block(dataBuffer,rq->wValue.bytes[0] ,rq->wValue.bytes[1] );
            usbMsgPtr = dataBuffer;         /* tell the driver which data to return */
            return rq->wValue.bytes[1];
        }
    }
    else if(rq->bRequest == USB_EEPROM_WRITE){
        //Überprüfe Bereich
        if( rq->wValue.bytes[0] < 128 ){
            eeprom_update_byte(rq->wValue.bytes[0],rq->wValue.bytes[1]);
        }
    }
    return 0;   /* default for not implemented requests: return no data back to host */
}

/* ------------------------------------------------------------------------- */


//Interrupt Service Routine for INT0
ISR(PCINT_vect)
{
    static volatile uint8_t pulseData = 0;
    register uint8_t i = PINB;
    //Zähle nur steigende Flanke
    register uint8_t mod = (i^pulseData)&i;
    pulseData = i;

    i = 7;
    do{
        if(mod & (1<<i)){
            pulseCount[i]++;
        }
    }while(i--);
}



int __attribute__((noreturn)) main(void)
{
    uchar   i;

    wdt_enable(WDTO_1S);


    /* Even if you don't use the watchdog, turn it off here. On newer devices,
     * the status of the watchdog (on/off, period) is PRESERVED OVER RESET!
     */
    /* RESET status: all port bits are inputs without pull-up.
     * That's the way we need D+ and D-. Therefore we don't need any
     * additional hardware initialization.
     */
    usbInit();
    usbDeviceDisconnect();  /* enforce re-enumeration, do this while interrupts are disabled! */

    ACSR = (1<<ACD); //Deaktiviere comparator

    //Initialisiere PORTB
    PORTB = 0xFF;
    PCMSK = 0xFF;
    GIMSK |=(1<<PCIE);

    PORTD |= (1<<PD4)|(1<<PD5);
    //Initialisiere Timer0 als counter auf T0 (PD4)
    TCCR0B = (1<<CS02) | (1<<CS01) | (1<<CS00);

    //Initialisiere Timer 1 als counter auf T1 (PD5)
    TCCR1B = (1<<CS02) | (1<<CS01) | (1<<CS00);

    //Initialisiere Uart auf möglichst langen transfer für led blinken
    UCSRB = (1<<UCSZ2) | (1<<TXEN);
    UCSRC = (1<<UPM1) | (1<<UCSZ0) | (1<<UCSZ1); //9bit Transfer + Even parity
    UBRRL = 0xFF;
    UBRRH = 0x0F;


    i = 0;
    while(--i){             /* fake USB disconnect for > 250 ms */
        wdt_reset();
        _delay_ms(1);
    }

    usbDeviceConnect();

    sei();

    for(;;){                /* main event loop */
        wdt_reset();
        usbPoll();
    }
}

/* ------------------------------------------------------------------------- */
